import React from 'react';
import { useState } from 'react';
import { TableContainer, 
  Paper, 
  Table, 
  TableHead, 
  TableBody, 
  TableRow, 
  TableCell, 
  Snackbar, 
  Backdrop, 
  useMediaQuery } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';

import { DateTime } from 'luxon';
import { LinearProgress, CircularProgress } from '@material-ui/core';

import useInterval from './hooks/useInterval';

type TrainDataState = {
  scraped: Array<TrainData>,
  scrape_time: string,
  status: string
}

type TrainData = {
  train_class: string,
  train_number: string,
  train_name: string
  train_from: string,
  train_to: string,
  delay_to: string,
  delay_from: string,
  cancelled_from: string,
  cancelled_to: string,
  actual_delay: string,
  status: string
}

let getRoute = (train: TrainData) => {
  let from, to = ""
  if (train.actual_delay.indexOf("Odwołany") !== -1) {
    from = train.cancelled_from
    to = train.cancelled_to
  } else {
    from = train.delay_from
    to = train.delay_to
  }
  return `${from}${from !== to ? ` - ${to}` : ""}`
}

interface AlertContentProps {
  status: string,
  scrapeTime: string
}

const HIDE_ALERT_AFTER_MS = 10000;

function AlertContent({ status, scrapeTime }: AlertContentProps) {
  const [percent, setPercent] = useState<number>(0);

  useInterval(() => setPercent(percent + 1), (status == "done" && percent < 100) ? HIDE_ALERT_AFTER_MS / 100 : null)

  if(status === "done") {

    return (
    <>
      <Alert severity="success">
      <div>Opóźnienia ciepłe jak bułki z biedry</div>
      <div>Z: <b>{scrapeTime !== "0" ? DateTime.fromISO(scrapeTime).toLocaleString(DateTime.DATETIME_SHORT) : ""}</b></div>
      </Alert>
      <LinearProgress variant="determinate" value={percent} />
    </>
    );
  }
  if(status === "exception") {
    return (<Alert severity="error">
        <div>Whoopsie, pepeś nas nie kocha i coś mogło pójść nie tak.</div>
        <div>Potencjalnie niekompletne dane zaznaczono na czerwono.</div>
      </Alert>);
  }
  return (<Alert severity="info"><div>Jeszcze miele, zaczekej.</div></Alert>);
}

function AppContent() {
  const [trainDataState, setTrainDataState] = useState<TrainDataState>({scraped: [], scrape_time: "0", status: "pending"});
  const [noAlert, setNoAlert] = useState<Boolean>(false);
  const [scheded, setScheded] = useState<Boolean>(false);

  const isFetchingDone = (status: string) => ["done", "exception"].indexOf(status) !== -1

  const getData = () => {
    if(!scheded) {
      return () => {
        setScheded(true)
        fetch("/api/sched");
      }
    }
    return () => {
      fetch("/api/last_scrape").then((data) => data.json()).then((data) => {
        let timeDiff = (new Date()).getTime() - (new Date(data.last_scrape)).getTime()
        if (timeDiff < 60000) {
          fetch("/api/get").then((data) => data.json()).then((data) => {
            if(data.status !== trainDataState.status || !isFetchingDone(trainDataState.status)) {
              setTrainDataState(data);
            }
          });
        }
      });
    }
  }

  useInterval(getData(), isFetchingDone(trainDataState.status) ? null : 1000);

  useInterval(() => setNoAlert(true), !isFetchingDone(trainDataState.status) ? null : HIDE_ALERT_AFTER_MS);  

  return (
    <>
    {trainDataState.scraped.length === 0 ? 
    <Backdrop open={true}>
      <div style={{display: "flex", flexDirection: "column", alignItems: "center"}}>
        <CircularProgress color="inherit"/>
        <div style={{color: "white", paddingTop: "1em"}}><h4>Odświeżamy buły z biedry</h4></div>
      </div>
    </Backdrop> : null}
    <TableContainer component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Lp</TableCell>
            <TableCell>Kategoria</TableCell>
            <TableCell>Numer</TableCell>
            <TableCell>Nazwa</TableCell>
            <TableCell>Trasa</TableCell>
            <TableCell>Opóźnienie</TableCell>
            <TableCell>Gdzie</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {trainDataState.scraped.map((train, idx) => (<TableRow style={train.status === "exception" ? {background: "#aa0000"} : {}}>
            <TableCell>{train.actual_delay.indexOf("Odwołany") === -1 ? idx + 1 - trainDataState.scraped.filter((singleTrain) => singleTrain.actual_delay.indexOf("Odwołany") !== -1).length : "-"}</TableCell>
            <TableCell>{train.train_class}</TableCell>
            <TableCell>{train.train_number}</TableCell>
            <TableCell>{train.train_name}</TableCell>
            <TableCell>{train.train_from} – {train.train_to}</TableCell>
            <TableCell>{train.actual_delay.replace("Min", "min")}</TableCell>
            <TableCell>{getRoute(train)}</TableCell>
          </TableRow>))}
        </TableBody>
      </Table>
    </TableContainer>
    <Snackbar open={trainDataState.scrape_time != "0" && !noAlert}>
      <AlertContent status={trainDataState.status} scrapeTime={trainDataState.scrape_time} />
    </Snackbar>
    </>
  );
}

function App() {

  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');

  const theme = React.useMemo(
    () =>
      createMuiTheme({
        palette: {
          type: prefersDarkMode ? 'dark' : 'light',
        },
      }),
    [prefersDarkMode],
  );

  return (<ThemeProvider theme={theme}><AppContent /></ThemeProvider>)
}

export default App;
