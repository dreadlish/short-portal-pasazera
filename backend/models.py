from mongoengine import Document, EmbeddedDocument, StringField, IntField, EmbeddedDocumentListField, DateTimeField, ObjectIdField
from bson.objectid import ObjectId

class StatusedDocument:
    status = StringField(required=True)
    meta = {"allow_inheritance": True}

class Train(StatusedDocument, EmbeddedDocument):
    oid = ObjectIdField(required=True, default=ObjectId,
                    unique=True, primary_key=True)
    # order_id = IntField(required=True, min_value=1)
    train_class = StringField()
    train_name = StringField()
    train_number = StringField()
    train_from = StringField()
    train_to = StringField()
    actual_delay = StringField()
    cancelled_from = StringField()
    cancelled_to = StringField()
    delay_from = StringField()
    delay_to = StringField()
    difficulties = StringField()

# class Page(StatusedDocument, EmbeddedDocument):
#     oid = ObjectIdField(required=True, default=ObjectId,
#                     unique=True, primary_key=True)
#     trains = EmbeddedDocumentListField(Train)
#     page_id = IntField(required=True, min_value=1)

class WholePortal(StatusedDocument, Document):
    last_scrape = DateTimeField()
    # pages = EmbeddedDocumentListField(Page)
    trains = EmbeddedDocumentListField(Train)