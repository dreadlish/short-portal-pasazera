from flask import Flask, jsonify

from bson import json_util

# import redis
from flask_mongoengine import MongoEngine
import models
import datetime
from celery.app.control import Inspect

import scrapper
import json

app = Flask(__name__)
app.config['MONGODB_SETTINGS'] = {
    'host': 'mongodb://localhost/intersraczka'
}
# CORS(app)
db = MongoEngine(app)

def last_scrape_time():
    wholeportal_objects = models.WholePortal.objects
    if not len(wholeportal_objects) or wholeportal_objects[0].last_scrape is None:
        return datetime.datetime.fromtimestamp(0)
    return wholeportal_objects[0].last_scrape

@app.route("/api/last_scrape")
def last_scrape():
    return jsonify({"last_scrape": last_scrape_time().isoformat()})

def plan_scrape():
    inspector = scrapper.app.control.inspect()

    delta = datetime.datetime.now() - last_scrape_time()

    if delta > datetime.timedelta(seconds=60):
        act = inspector.active()
        if not len(list(act.values())[0]):
            scrapper.scrap.delay()

@app.route("/api/sched")
def schedule():
    plan_scrape()
    return jsonify({"status": "OK"})

@app.route("/api/get")
def get_delays():
    if not models.WholePortal.objects:
        return jsonify({})
    return jsonify({"scraped": json.loads(json_util.dumps([train.to_mongo().to_dict() for train in models.WholePortal.objects[0].trains])),
        "scrape_time": last_scrape_time().isoformat(),
        "status": models.WholePortal.objects[0].status})
    # return jsonify({"scraped": json.loads(redis.Redis().get("pp_scrapped").decode()), "scrape_time": last_scrape_time().isoformat()})

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=9020)