from celery import Celery

import logging

import requests
from bs4 import BeautifulSoup, element
from redis import Redis

import json

import billiard as multiprocessing

import datetime

import models

import mongoengine

app = Celery('pp-scraper', broker='redis://localhost:6379/0')

PORTAL_PASAZERA_URL = "https://portalpasazera.pl"

PORTAL_PASAZERA_DELAY_URL = f"{PORTAL_PASAZERA_URL}/Opoznienia?s=4"

DIVS_ENUM = ("class", "name", "number", "route", "delay")


def determine_sites_amount(text: str) -> int:
    site_soup = BeautifulSoup(text, features="lxml")
    pager_links_select = site_soup.select(".pagination>li>.loadScr")
    return int(pager_links_select[-2].text)

def fetch_data_from_train_details(link, train_class):
    details = {}
    details_page_req = requests.get(f"{PORTAL_PASAZERA_URL}{link}")
    details_page_req.raise_for_status()

    details_soup = BeautifulSoup(details_page_req.text, features="lxml")
    try:
        if train_class == "IC":
            details["train_class"] = get_ic_class(details_soup.select(".loadScr.abt-n-db")[3]["href"])
    except:
        pass # literally don't do anything, because why.

    delay_place = get_delay_place(details_soup)
    details["delay_from"] = delay_place["delay_from"]
    details["delay_to"] = delay_place["delay_to"]

    cancelled_place = get_cancelled_place(details_soup)
    details["cancelled_from"] = cancelled_place["cancelled_from"]
    details["cancelled_to"] = cancelled_place["cancelled_to"]

    return details

def get_delay_place(details_soup):
    above_15_minutes = details_soup.select(".timeline")[0].select(".timeline__item--delay-above-15")
    delay_from = ""
    delay_to = ""
    if above_15_minutes:
        delay_from = list(above_15_minutes[0].select(".timeline__content-station")[0].children)[-1].strip()
        delay_to = list(above_15_minutes[-1].select(".timeline__content-station")[0].children)[-1].strip()
    return {"delay_from": delay_from, "delay_to": delay_to}

def get_cancelled_place(details_soup):
    cancelled = details_soup.select(".timeline__content__detour")
    if cancelled:
        cancelled = cancelled[0].select(".timeline__item")
    cancelled_from = ""
    cancelled_to = ""
    if cancelled:
        cancelled_from = list(cancelled[0].select(".timeline__content-station")[0].children)[-1].strip()
        cancelled_to = list(cancelled[-1].select(".timeline__content-station")[0].children)[-1].strip()
    return {"cancelled_from": cancelled_from, "cancelled_to": cancelled_to}
    

def get_ic_class(train_details_link):
    train_details_req = requests.get(f"{PORTAL_PASAZERA_URL}{train_details_link}")
    train_details_req.raise_for_status()

    train_details_soup = BeautifulSoup(train_details_req.text, features="lxml")
    train_details_row = train_details_soup.select(".row.connection-details__item-row")[0]

    divs = train_details_row.select(".col-10>div")
    return divs[0].select(".item-value")[-1].text

def parse_row(row: element.Tag) -> dict:
    divs = row.select("div>.item-value")
    train = {}
    for idx, single_div in enumerate(divs):
        if idx == 0 :
            train[DIVS_ENUM[idx]] = list(single_div.children)[4].strip().lstrip()
            continue
        if idx != 3:
            train[DIVS_ENUM[idx]] = single_div.text
        else:
            train["train_from"] = single_div.select("span")[0].text
            train["train_to"] = single_div.select("span")[1].text
    train["details_link"] = row.select(".item-details.loadScr")[0]["href"]
    difficulties_buttons = row.select("button")
    train["difficulties"] = ""
    if difficulties_buttons:
        train["difficulties"] = difficulties_buttons[0].attrs['data-difficulties']
    return train

def scrap_single_page(text: str) -> list:
    site_soup = BeautifulSoup(text, features="lxml")
    rows = site_soup.select(".row.delays-table__row")
    parsed_rows = [parse_row(row) for row in rows]
    return parsed_rows


def process_page(num: int) -> list:
    times = 1
    while 1:
        try:
            request = requests.get(f"{PORTAL_PASAZERA_DELAY_URL}&p={num}")
            request.raise_for_status()
            break
        except requests.exceptions.HTTPError:
            times += 1
            if times >= 3:
                logging.error(f"Failed processing page no {num}")
                return [{"error": "connection_error", "page": num}]
    return scrap_single_page(request.text)

def clean_db_and_recreate(pages_no):
    for portal_obj in models.WholePortal.objects:
        portal_obj.delete()
    whole_portal = models.WholePortal()
    whole_portal.status = "processing"
    whole_portal.save()
    return whole_portal

def scrap_details(single_train):
    details = {}
    try:
        details = fetch_data_from_train_details(single_train['details_link'], single_train['class'])
        details["status"] = "done"
    except:
        logging.exception("Something is borken")
        details["status"] = "exception"
    return details

@app.task
def scrap():
    mongoengine.connect(host='mongodb://localhost/intersraczka')
    logging.info("Connected to mongo")
    first_request = requests.get(PORTAL_PASAZERA_DELAY_URL)
    first_request.raise_for_status()

    request_body = first_request.text
    logging.info("Got first request")
    pages = determine_sites_amount(request_body)
    logging.info(f"Got {pages} pages.")
    portal_object = clean_db_and_recreate(pages)
    logging.info("Recreated db")
    scrapped = scrap_single_page(request_body)
    
    mp_pool = multiprocessing.Pool(8)

    remaining_pages = mp_pool.map(process_page, range(2, pages+1))

    for page in remaining_pages:
        for train in page:
            if not [sc_train for sc_train in scrapped if sc_train['number'] == train["number"]]:
                scrapped.append(train)
    
    logging.info("Scrapped basic")
    for single_train in scrapped:
        portal_object.trains.append(models.Train(
            status="processing",
            train_class = single_train.get('class', ''),
            train_name = single_train.get('name', ''),
            train_number = single_train.get('number', ''),
            train_from = single_train.get('train_from', ''),
            train_to = single_train.get('train_to', ''),
            actual_delay = single_train.get('delay', ''),
            cancelled_from = '',
            cancelled_to = '',
            delay_from = '',
            delay_to = '',
            difficulties = single_train.get('difficulties', '')
        ))

    portal_object.last_scrape = datetime.datetime.now()
    portal_object.save()

    logging.info("Updated db")
    logging.info("Scrapping details data")

    statuses = {json.dumps(single_train): mp_pool.apply_async(scrap_details, (single_train, )) for single_train in scrapped}
    overall_status = "done"
    while 1:
        done_statuses = {train: statuses[train] for train in statuses if statuses[train].ready()}
        for train in done_statuses:
            details = done_statuses[train].get()
            if details["status"] == "exception":
                overall_status = "exception"
            prepared_details = {f"set__trains__S__{k}": details[k] for k in details}
            single_train_obj = json.loads(train)
            models.WholePortal.objects(trains__train_number=single_train_obj['number']).update(**prepared_details)
            statuses.pop(train)
        if not len(statuses):
            break
    models.WholePortal.objects[0].update(set__status=overall_status)

    logging.info("Done")

    mongoengine.disconnect()

    logging.info("Disconnecting mongo")
